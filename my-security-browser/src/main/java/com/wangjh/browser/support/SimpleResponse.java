package com.wangjh.browser.support;

/**
 * @author wangJH
 * @createdate 2018/7/31.
 */
public class SimpleResponse {

    private Object content;

    public SimpleResponse() {
    }

    public SimpleResponse(Object content) {
        this.content = content;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }
}
