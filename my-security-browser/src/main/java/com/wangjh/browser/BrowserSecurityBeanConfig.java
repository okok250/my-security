package com.wangjh.browser;

import com.wangjh.browser.session.MySessionExpiredStrategy;
import com.wangjh.browser.session.MySessionInvalidStrategy;
import com.wangjh.core.properties.SecurityProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.session.InvalidSessionStrategy;
import org.springframework.security.web.session.SessionInformationExpiredStrategy;

/**
 * @author wangJH
 * @createdate 2018/8/3.
 */
@Configuration
public class BrowserSecurityBeanConfig {

    @Autowired
    private SecurityProperties securityProperties;

    @Bean
    @ConditionalOnMissingBean(value = InvalidSessionStrategy.class)
    public InvalidSessionStrategy invalidSessionStrategy() {
        MySessionInvalidStrategy strategy = new MySessionInvalidStrategy(securityProperties.getBrowser().getSession().getSessionInvalidUrl());
        return strategy;
    }

    @Bean
    @ConditionalOnMissingBean(value = SessionInformationExpiredStrategy.class)
    public SessionInformationExpiredStrategy sessionInformationExpiredStrategy() {
        MySessionExpiredStrategy strategy = new MySessionExpiredStrategy(securityProperties.getBrowser().getSession().getSessionInvalidUrl());
        return strategy;
    }

}
