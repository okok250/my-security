package com.wangjh.browser;

import com.wangjh.browser.support.SimpleResponse;
import com.wangjh.core.properties.SecurityConstants;
import com.wangjh.core.properties.SecurityProperties;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author wangJH
 * @createdate 2018/7/31.
 */
@RestController
public class BrowserSecurityCtrl {

    private static final Logger log = LoggerFactory.getLogger(BrowserSecurityCtrl.class);

    private RequestCache requestCache = new HttpSessionRequestCache();

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Autowired
    private SecurityProperties securityProperties;
    /**
     * 需要身份认证时，跳转到这里
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(SecurityConstants.DEFAULT_UNAUTHENTICATION_URL)
    @ResponseStatus(code = HttpStatus.UNAUTHORIZED)
    public SimpleResponse requireAuthentication(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SavedRequest savedRequest = requestCache.getRequest(request, response);
        if (savedRequest != null) {
            String target = savedRequest.getRedirectUrl();
            log.info("引发跳转的请求是：{}", target);
            if (StringUtils.endsWithIgnoreCase(target, ".html")) {
                redirectStrategy.sendRedirect(request, response,securityProperties.getBrowser().getLoginPage());
                return null;
            }
        }
        return new SimpleResponse("访问服务需要身份认证，请引导用户到登录页！");
    }

//    @GetMapping(SecurityConstants.DEFAULT_SESSION_INVALID_URI)
//    @ResponseStatus(HttpStatus.UNAUTHORIZED)
//    public SimpleResponse sessionInvalid() {
//        return new SimpleResponse("session失效！");
//    }
}
