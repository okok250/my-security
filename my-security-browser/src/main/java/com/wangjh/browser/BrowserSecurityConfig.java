package com.wangjh.browser;

import com.wangjh.core.authentication.AbstractChannelSecurityConfig;
import com.wangjh.core.authentication.mobile.SmsCodeAuthenticationSecurityConfig;
import com.wangjh.core.properties.SecurityConstants;
import com.wangjh.core.properties.SecurityProperties;
import com.wangjh.core.validate.code.core.ValidateCodeSecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.session.InvalidSessionStrategy;
import org.springframework.security.web.session.SessionInformationExpiredStrategy;

import javax.sql.DataSource;

/**
 * 封装过后的主要配置类
 *
 * 继承自一个抽象类，抽象类继承了WebSecurityConfigurerAdapter类，把公共的部分封装在一起
 * @author wangJH
 * @createdate 2018/7/31.
 */
@Configuration
@EnableWebSecurity
public class BrowserSecurityConfig extends AbstractChannelSecurityConfig {

    @Autowired
    private SecurityProperties securityProperties;//可配置的相关属性
    @Autowired
    private DataSource dataSource;//数据源----rememberme使用到
    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private InvalidSessionStrategy invalidSessionStrategy;
    @Autowired
    private SessionInformationExpiredStrategy sessionInformationExpiredStrategy;
    /**
     * 下面两个过配置中使用的过滤器重点注意
     * 图片验证码登录会使用username，是security支持的认证方式，只需要添加验证码校验的过滤器
     * 短信验证码登录不是security支持的方式，所以需要自己完成它的认证过滤器
     * 这里面两类过滤器的过滤顺序-----先校验---再认证
     */
    @Autowired
    private ValidateCodeSecurityConfig validateCodeSecurityConfig;//验证码生成和校验配置
    @Autowired
    private SmsCodeAuthenticationSecurityConfig smsCodeAuthenticationSecurityConfig;//短信登录认证配置

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * rememberme功能使用security自带的实现方式
     * @return
     */
    @Bean
    public PersistentTokenRepository persistentTokenRepository() {
        JdbcTokenRepositoryImpl repository = new JdbcTokenRepositoryImpl();
        repository.setDataSource(dataSource);
//        第一次运行时需要开启，生成表之后，必须关闭
//        repository.setCreateTableOnStartup(true);
        return repository;
    }

    /**
     * 核心配置方法
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        applyPasswordAuthenticationConfig(http);

        http.csrf().disable()
                    .sessionManagement()
                    .invalidSessionStrategy(invalidSessionStrategy)
                    .maximumSessions(securityProperties.getBrowser().getSession().getMaximumSessions())
                    .expiredSessionStrategy(sessionInformationExpiredStrategy)
//                阻止后一次重复登录的行为
                    .maxSessionsPreventsLogin(securityProperties.getBrowser().getSession().isMaxSessionPreventsLogin())
                .and()
                .and()
//                    .logout()
//                    .logoutUrl()
//                    .logoutSuccessUrl()
//                    .logoutSuccessHandler()
                    .apply(validateCodeSecurityConfig)
                .and()
                    .apply(smsCodeAuthenticationSecurityConfig)
                .and()
                    .rememberMe()
                    .tokenRepository(persistentTokenRepository())
                    .tokenValiditySeconds(securityProperties.getBrowser().getRememberMeSeconds())
                    .userDetailsService(userDetailsService)
                .and()
                    .authorizeRequests()
                    .antMatchers(
                            securityProperties.getBrowser().getLoginPage(),
                            securityProperties.getBrowser().getSession().getSessionInvalidUrl()+".json",
                            securityProperties.getBrowser().getSession().getSessionInvalidUrl()+".html",
                            SecurityConstants.DEFAULT_UNAUTHENTICATION_URL,
                            SecurityConstants.DEFAULT_LOGIN_PROCESSING_URL_MOBILE,
                            SecurityConstants.DEFAULT_SESSION_INVALID_URL,
                            SecurityConstants.DEFAULT_VALIDATE_CODE_URL_PREFIX + "/*")
                    .permitAll()
                    .anyRequest().authenticated();
    }
}
