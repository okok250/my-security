package com.wangjh.demo.web;

import com.wangjh.demo.exception.UserNotExistException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * 统一异常处理
 * @author SYSTEM
 */
@ControllerAdvice
public class CtrlExcpetionHandler {

    /**
     * 只处理产生<b>UserNotExistException.class</b>异常的ctrl
     * 返回状态码为500
     * @param e
     * @return
     */
    @ExceptionHandler(value = UserNotExistException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Map<String,Object> handleUserNotExsitException(UserNotExistException e){
        HashMap<String, Object> map = new HashMap<>();
        map.put("id", e.getId());
        map.put("message", e.getMessage());
        return map;
    }

}
