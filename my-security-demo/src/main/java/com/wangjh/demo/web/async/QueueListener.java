package com.wangjh.demo.web.async;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * @author wangJH
 * @createdate 2018/7/31.
 */
@Component
public class QueueListener implements ApplicationListener<ContextRefreshedEvent> {

    private static final Logger log = LoggerFactory.getLogger(QueueListener.class);

    @Autowired
    private MockQueue queue;

    @Autowired
    private DeferredResultHolder deferredResultHolder;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        new Thread(()->{
            while (true) {
                if (StringUtils.isNotBlank(queue.getCompleteOrder())) {
                    String order = queue.getCompleteOrder();
                    log.info("返回订单处理结果：{}",order);
                    deferredResultHolder.getMap().get(order).setResult("place order success!");
                    queue.setCompleteOrder(null);
                }else {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
}
