package com.wangjh.demo.web;

import com.fasterxml.jackson.annotation.JsonView;
import com.wangjh.demo.dto.User;
import com.wangjh.demo.dto.UserQueryCondition;
import com.wangjh.demo.exception.UserNotExistException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * user控制器
 * 在spring boot默认的处理方式中，会根据请求的类型返回对应的结果
 * 在BasicErrorController中，请求头中携带"accept=text/html"参数，则返回html视图，其他则返回json对象
 * 再返回html视图的方法里，可以跳转自定义的错误页面，但存放位置可能因版本而异，当前版本需要存放在/resources/static/error/下
 *
 * @author wangJH
 * @createdate 2018/7/30.
 */
@RestController
@RequestMapping("user")
public class UserCtrl {

    @GetMapping("/me")
    public Object getCurrentUser(@AuthenticationPrincipal UserDetails userDetails) {
//        return SecurityContextHolder.getContext().getAuthentication();
//        return authentication;
        return userDetails;
    }

    @PreAuthorize("hasAnyRole('SUPER')")
    @GetMapping
    @JsonView(User.UserSimpleView.class)
    @ApiOperation(value = "查询用户列表")
    public List<User> queryUsers(UserQueryCondition condition,
                                 @PageableDefault(page = 1, size = 15, sort = "username", direction = Sort.Direction.ASC) Pageable pageable) {
        System.out.println("condition = " + condition);
        System.out.println("pageable = " + pageable.toString());
        List<User> users = new ArrayList<>();
        users.add(new User());
        users.add(new User());
        users.add(new User());
        return users;
    }

    /**
     * 在使用@PathVariable注解的方法中，可以使用正则表达式
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/{id:\\d+}")
    @JsonView(User.UserDetailView.class)
    public User getInfo(@ApiParam("用户ID") @PathVariable("id") String id) {
//        throw new UserNotExistException(id);
        System.out.println("进入getInfo服务");
        User user = new User();
        user.setId("tom");
        return user;
    }

    @PostMapping
    @JsonView(User.UserSimpleView.class)
    public User createUser(@RequestBody @Valid User user) {

        System.out.println("user = " + user);
        user.setId("1");
        return user;
    }

    /**
     * 在使用了@Valid校验的方法中，
     * 如果没有使用BindingResult作为入参，校验不通过则不会进入方法
     * 如果使用了，不通过也能进入方法中并继续进行
     *
     * @param user
     * @param errors
     * @return
     */
    @PutMapping("/{id:\\d+}")
    @JsonView(User.UserSimpleView.class)
    public User updateUser(@RequestBody @Valid User user, BindingResult errors) {

        if (errors.hasErrors()) {
            errors.getAllErrors().stream().forEach(x -> System.err.println(x.getDefaultMessage()));
        }

        System.out.println("user = " + user);
        user.setId("1");
        return user;
    }

    @DeleteMapping("/{id:\\d+}")
    @JsonView(User.UserSimpleView.class)
    public void deleteUser(@PathVariable("id") String id) {
        System.out.println("id = " + id);
    }

}
