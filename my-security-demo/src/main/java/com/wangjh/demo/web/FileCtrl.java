package com.wangjh.demo.web;

import com.wangjh.demo.dto.FileInfo;
import org.apache.commons.io.IOUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * 文件上传下载——简单处理
 * @author wangJH
 * @createdate 2018/7/31.
 */
@RestController
@RequestMapping("/file")
public class FileCtrl {

    private String folder = "E:\\gitrepo\\my-security\\my-security-demo\\src\\main\\java\\com\\wangjh\\demo\\web";

    @PostMapping
    public FileInfo upload(MultipartFile file) throws IOException {
        System.out.println("file = " + file.getName());
        System.out.println("file = " + file.getOriginalFilename());
        System.out.println("file.getSize() = " + file.getSize());
        File localFile = new File(folder, System.currentTimeMillis() + ".txt");
        file.transferTo(localFile);
        return new FileInfo(localFile.getAbsolutePath());
    }

    @GetMapping("/{id}")
    public void download(@PathVariable String id,
                         HttpServletRequest request,
                         HttpServletResponse response) {
        try (InputStream is = new FileInputStream(new File(folder, id + ".txt"));
             OutputStream os = response.getOutputStream();) {
            response.setContentType("application/x-download");
            response.addHeader("Content-Disposition","attachement;filename=test.txt");
            IOUtils.copy(is, os);
            os.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
