package com.wangjh.demo.web.async;

import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.concurrent.Callable;

/**
 * @author wangJH
 * @createdate 2018/7/31.
 */
@RestController
public class AsyncCtrl {

    private static final Logger log = LoggerFactory.getLogger(AsyncCtrl.class);

    @Autowired
    private MockQueue mockQueue;

    @Autowired
    private DeferredResultHolder deferredResultHolder;

    @GetMapping("order")
    public DeferredResult<String> order() throws InterruptedException {
        log.info("主线程开始！");
        String order = RandomStringUtils.randomNumeric(8);
        mockQueue.setPlaceOrder(order);
        DeferredResult<String> result = new DeferredResult<>();
        deferredResultHolder.getMap().put(order, result);

//        Callable<String> callable = () -> {
//            log.info("副线程开始！");
//            Thread.sleep(1000);
//            log.info("副线程返回！");
//            return "success";
//        };
        log.info("主线程结束！");
        return result;
    }
}
