package com.wangjh.demo.web.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * @author wangJH
 * @createdate 2018/7/31.
 */
//@Component
public class TimeFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("TimeFilter init!");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("TimeFilter start!");
        long start = System.currentTimeMillis();
        filterChain.doFilter(servletRequest,servletResponse);
        System.out.println("TimeFilter time:"+(System.currentTimeMillis()-start));
        System.out.println("TimeFilter finish!");
    }

    @Override
    public void destroy() {
        System.out.println("TimeFilter destroy!");
    }
}
