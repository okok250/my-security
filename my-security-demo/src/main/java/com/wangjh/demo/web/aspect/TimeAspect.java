package com.wangjh.demo.web.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @author wangJH
 * @createdate 2018/7/31.
 */
//@Component
//@Aspect
public class TimeAspect {

    @Around("execution(* com.wangjh.demo.web.UserCtrl.*(..))")
    public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
        System.err.println("time aspect start！");
        long start = System.currentTimeMillis();
        Object[] args = pjp.getArgs();
        Arrays.stream(args).forEach(System.out::println);
        Object o = pjp.proceed();

        System.err.println("time aspect 耗时：" + (System.currentTimeMillis() - start));
        System.err.println("time aspect end！");
        return o;
    }
}
