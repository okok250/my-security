package com.wangjh.demo.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.social.security.SocialUser;
import org.springframework.social.security.SocialUserDetails;
import org.springframework.social.security.SocialUserDetailsService;
import org.springframework.stereotype.Component;

/**
 * @author wangJH
 * @createdate 2018/7/31.
 */
@Component
public class MyUserDetailsService implements UserDetailsService , SocialUserDetailsService {

    private static final Logger log = LoggerFactory.getLogger(MyUserDetailsService.class);

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.info("表单登录！：用户名{}",username);
        //TODO:根据有户名查找信息
//        TODO:根据已有信息，判断是否冻结，过期等状态
        return getUserDetails(username);
    }

    private SocialUser getUserDetails(String username) {
        String password = passwordEncoder.encode("123456");
        return new SocialUser(username,password,
                true, true,
                true, true,
                AuthorityUtils.commaSeparatedStringToAuthorityList("admin"));
    }

    @Override
    public SocialUserDetails loadUserByUserId(String userId) throws UsernameNotFoundException {
        log.info("社交登陆:用户ID：{}",userId);
        //TODO:根据有户名查找信息
//        TODO:根据已有信息，判断是否冻结，过期等状态
        return getUserDetails(userId);
    }
}
