package com.wangjh.demo.validator;

import com.wangjh.demo.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 自定义hibernate validator
 * 继承<b>ConstraintValidator</b>后，无需添加@Component之类的注解，spring会自动管理这个类
 * 该类中可以注入spring所管理的bean
 * @author wangJH
 */
public class MyConstraintValidator implements ConstraintValidator<MyConstraint,String> {

    @Autowired
    private HelloService helloService;

    @Override
    public void initialize(MyConstraint myConstraint) {
        System.out.println("init my validator!");
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        helloService.greeting("tom");
        System.out.println("s = " + s);
        return false;
    }
}
