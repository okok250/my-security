package com.wangjh.demo.dto;

/**
 * @author wangJH
 * @createdate 2018/7/31.
 */
public class FileInfo {
    private String path;

    public FileInfo(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
