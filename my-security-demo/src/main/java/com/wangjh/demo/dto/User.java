package com.wangjh.demo.dto;

import com.fasterxml.jackson.annotation.JsonView;
import com.wangjh.demo.validator.MyConstraint;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Past;
import java.util.Date;

/**
 * 用户对象
 * @author wangJH
 * @createdate 2018/7/30.
 */
public class User {

    /**
     * <b>JsonView</b>所需要的json视图
     * 继承关系中，子类视图会展示父类视图的参数
     * 使用方法: 1.创建视图接口
     *          2.在对象属性上注解@JsonView，在注解中加上视图的接口
     *          3.在controller方法上注解@JsonView，在注解中加上视图的接口
     */
    public interface UserSimpleView{};
    public interface UserDetailView extends UserSimpleView{};

    private String id;

    @MyConstraint(message = "用户名测试")
    private String username;

    @NotBlank(message = "密码不能为空！")
    private String password;

    @Past(message = "生日必须是过去时间！")
    private Date birthday;

    @JsonView(UserSimpleView.class)
    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @JsonView(UserSimpleView.class)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @JsonView(UserDetailView.class)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @JsonView(UserSimpleView.class)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", birthday=" + birthday +
                '}';
    }
}
