package com.wangjh.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * scanBasePackages = "com.wangjh" 不加上扫描不到browser模块里的配置类
 * @author wangJH
 * @createdate 2018/7/30.
 */
@SpringBootApplication(scanBasePackages = "com.wangjh")
@RestController
@EnableSwagger2
//@EnableGlobalMethodSecurity(prePostEnabled = true)
public class MyDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyDemoApplication.class, args);
    }

    @GetMapping("hello")
    public String hello() {
        return "hello security!";
    }
}
