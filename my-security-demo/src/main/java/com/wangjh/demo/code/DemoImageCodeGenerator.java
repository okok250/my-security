package com.wangjh.demo.code;

import com.wangjh.core.validate.code.image.ImageCode;
import com.wangjh.core.validate.code.core.ValidateCodeGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * @author wangJH
 * @createdate 2018/8/1.
 */
//@Component("imageCodeGenerator")
public class DemoImageCodeGenerator implements ValidateCodeGenerator {

    private static final Logger log = LoggerFactory.getLogger(DemoImageCodeGenerator.class);

    @Override
    public ImageCode generate(ServletWebRequest request) {
        log.warn("更高级的，或自定义的图形验证码生成！");
        return null;
    }
}
