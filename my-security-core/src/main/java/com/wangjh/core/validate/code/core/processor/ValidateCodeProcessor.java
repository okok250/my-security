package com.wangjh.core.validate.code.core.processor;

import org.springframework.web.context.request.ServletWebRequest;

/**
 * 验证码流程处理接口
 * @author wangJH
 * @createdate 2018/8/1.
 */
public interface ValidateCodeProcessor {

    /**
     * session名称前缀
     */
    String SESSION_KEY_PREFIX = "SESSION_KEY_FOR_CODE_";

    /**
     * 创建验证码
     * @param request
     * @throws Exception
     */
    void create(ServletWebRequest request) throws Exception;
    /**
     * 校验验证码
     *
     * @param request
     * @throws Exception
     */
    void validate(ServletWebRequest request);
}
