package com.wangjh.core.validate.code.exception;


import org.springframework.security.core.AuthenticationException;

/**
 * @author wangJH
 * @createdate 2018/8/1.
 */
public class ValidateCodeException extends AuthenticationException {
    public ValidateCodeException(String detail) {
        super(detail);
    }
}
