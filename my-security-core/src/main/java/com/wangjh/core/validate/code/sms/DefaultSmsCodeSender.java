package com.wangjh.core.validate.code.sms;

/**
 * 默认的短信发送对象
 * @author wangJH
 * @createdate 2018/8/1.
 */
public class DefaultSmsCodeSender implements SmsCodeSender {
    @Override
    public void send(String mobile, String code) {
        System.out.println("模拟发送验证码，手机号："+mobile+"，验证码："+code);
    }
}
