package com.wangjh.core.validate.code.sms;

/**
 * @author wangJH
 * @createdate 2018/8/1.
 */
public interface SmsCodeSender {
    void send(String mobile, String code);
}
