package com.wangjh.core.validate.code;

import com.wangjh.core.validate.code.core.ValidateCodeProcessorHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author wangJH
 * @createdate 2018/8/1.
 */
@RestController
public class ValidateCtrl {

    /**
     * 设计思路：
     *      利用多态的特性，在项目中装配的是其子类，
     *          但子类的来源有两处：
     *              一是默认配置（见ValidateCodeBeanConfig类），如果不存在别的实现类（使用的别名相同，或类型，可自己定义）,则装配默认配置
     *              二是重新实现这个接口，并在把新的实现类加入spring的管理（按规则配置，例如是否需要加入别名）,则装配新的实现类
     */
    @Autowired
    private ValidateCodeProcessorHolder validateCodeProcessorHolder;

    @GetMapping("/code/{type}")
    public void codeCreate(HttpServletRequest req, HttpServletResponse resp,
                           @PathVariable String type) throws Exception {
        validateCodeProcessorHolder.findValidateCodeProcessor(type).create(new ServletWebRequest(req,resp));
    }


}
