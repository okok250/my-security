package com.wangjh.core.validate.code.core;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 图片验证码基本对象
 * @author wangJH
 * @createdate 2018/8/1.
 */
public class ValidateCode implements Serializable {

    /**
     * 验证码
     */
    private String code;

    /**
     * 过期时间---一个时刻
     */
    private LocalDateTime expireTime;

    /**
     * 一个构造器，设置过期时间，单位秒
     * 把当前系统时间加上过期时间，得到最后的过期时刻
     * @param code
     * @param expireIn
     */
    public ValidateCode(String code, int expireIn) {
        this.code = code;
        this.expireTime = LocalDateTime.now().plusSeconds(expireIn);
    }

    public ValidateCode(String code, LocalDateTime expireTime) {
        this.code = code;
        this.expireTime = expireTime;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public LocalDateTime getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(LocalDateTime expireTime) {
        this.expireTime = expireTime;
    }


    /**
     * 判断是否过期
     * @return
     */
    public boolean isExpired() {
        return LocalDateTime.now().isAfter(expireTime);
    }
}
