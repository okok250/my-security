package com.wangjh.core.validate.code.image;

import com.wangjh.core.validate.code.core.processor.AbstractValidateCodeProcessor;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;

import javax.imageio.ImageIO;

/**
 * 图片验证码的处理流程
 * @author wangJH
 * @createdate 2018/8/1.
 */
@Component("imageValidateCodeProcessor")
public class ImageValidateCodeProcessor extends AbstractValidateCodeProcessor<ImageCode> {

    @Override
    protected void send(ServletWebRequest request, ImageCode code) throws Exception {
        ImageIO.write(code.getImage(), "JPEG", request.getResponse().getOutputStream());
    }
}
