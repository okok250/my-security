package com.wangjh.core.validate.code.core;

import com.wangjh.core.validate.code.core.ValidateCode;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * @author wangJH
 * @createdate 2018/8/1.
 */
public interface ValidateCodeGenerator {
    ValidateCode generate(ServletWebRequest request);
}
