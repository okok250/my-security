package com.wangjh.core.validate.code.core;

import com.wangjh.core.properties.SecurityProperties;
import com.wangjh.core.validate.code.image.ImageCodeGenerator;
import com.wangjh.core.validate.code.sms.DefaultSmsCodeSender;
import com.wangjh.core.validate.code.sms.SmsCodeSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 这个配置类的作用在于可以替换某单一功能接口的实现类
 * 注解@ConditionalOnMissingBean表示如果找不到某个类，则加载下面的bean，详细使用方法见ConditionalOnMissingBean代码
 *
 * 该方法的实现效果和直接在实现类上注解@Component是一样的，但前者替换时，开发者只需关注如何实现单一功能即可，不会影响整个架构
 *
 * @author wangJH
 * @createdate 2018/8/1.
 */
@Configuration
public class ValidateCodeBeanConfig {

    @Autowired
    private SecurityProperties securityProperties;

    @Bean
    @ConditionalOnMissingBean(name = "imageValidateCodeGenerator")
    public ValidateCodeGenerator imageValidateCodeGenerator() {
        ImageCodeGenerator generator = new ImageCodeGenerator();
        generator.setSecurityProperties(securityProperties);
        return generator;
    }

    @Bean
    @ConditionalOnMissingBean(SmsCodeSender.class)
    public SmsCodeSender smsCodeSender() {
        return new DefaultSmsCodeSender();
    }
}
