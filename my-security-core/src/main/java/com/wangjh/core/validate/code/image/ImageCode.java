package com.wangjh.core.validate.code.image;

import com.wangjh.core.validate.code.core.ValidateCode;

import java.awt.image.BufferedImage;
import java.time.LocalDateTime;

/**
 * 图片验证码基本对象
 * @author wangJH
 * @createdate 2018/8/1.
 */
public class ImageCode extends ValidateCode {

    /**
     * 图片对象
     */
    private BufferedImage image;

    /**
     * 一个构造器，设置过期时间，单位秒
     * 把当前系统时间加上过期时间，得到最后的过期时刻
     * @param image
     * @param code
     * @param expireIn
     */
    public ImageCode(BufferedImage image, String code, int expireIn) {
        super(code,expireIn);
        this.image = image;
    }

    public ImageCode(BufferedImage image, String code, LocalDateTime expireTime) {
        super(code,expireTime);
        this.image = image;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }
}
