package com.wangjh.core.properties;

import org.springframework.boot.autoconfigure.social.SocialProperties;

/**
 * @author wangJH
 * @createdate 2018/8/3.
 */
public class QQProperties extends SocialProperties {

    private String providerId = "qq";

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }
}
