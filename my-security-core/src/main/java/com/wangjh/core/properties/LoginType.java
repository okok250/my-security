package com.wangjh.core.properties;

/**
 * @author SYSTEM
 */

public enum LoginType {
    /**
     * 重定向
     */
    REDIRECT,
    /**
     * json
     */
    JSON
}
