package com.wangjh.core.properties;

/**
 * @author wangJH
 * @createdate 2018/8/3.
 */
public class SocialProperties {
    private QQProperties qq = new QQProperties();

    public QQProperties getQq() {
        return qq;
    }

    public void setQq(QQProperties qq) {
        this.qq = qq;
    }
}
