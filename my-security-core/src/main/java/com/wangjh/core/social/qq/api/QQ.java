package com.wangjh.core.social.qq.api;

/**
 * @author wangJH
 * @createdate 2018/8/3.
 */
public interface QQ {
    QQUserInfo getUserInfo();
}
