package com.wangjh.core.social.qq.connect;

import com.wangjh.core.social.qq.api.QQ;
import com.wangjh.core.social.qq.api.QQUserInfo;
import org.springframework.social.connect.ApiAdapter;
import org.springframework.social.connect.ConnectionValues;
import org.springframework.social.connect.UserProfile;

/**
 * @author wangJH
 * @createdate 2018/8/3.
 */
public class QQAdapter implements ApiAdapter<QQ> {

    @Override
    public boolean test(QQ qq) {
        return true;
    }

    @Override
    public void setConnectionValues(QQ qq, ConnectionValues connectionValues) {
        QQUserInfo info = qq.getUserInfo();
        connectionValues.setDisplayName(info.getNickname());
        connectionValues.setImageUrl(info.getFigureurl_qq_1());
        connectionValues.setProfileUrl(null);
        connectionValues.setProviderUserId(info.getOpenId());
    }

    @Override
    public UserProfile fetchUserProfile(QQ qq) {
        return null;
    }

    @Override
    public void updateStatus(QQ qq, String s) {
        //todo:在QQ里啥也不用做
    }
}
